"""
@title:     KP MCI - Server 
@author:    Felix Goebel
@date:      13.10.2023
"""

from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Employee(Base):
    __tablename__ = "employees"
    employee_id = Column("employee_id", Integer, primary_key=True, index=True)
    firstname = Column("firstname", String, nullable=False)
    lastname = Column("lastname", String,  nullable=False)
    last_seen = Column("last_seen", String)
    place_met = Column("place_met", String)
    role = Column("role", String)
    picture = Column("picture", String)