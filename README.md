# README
Author: Felix Goebel
Last updated: 16.10.2023

## Virtual Environment [optional]

Create virtual environment, then activate:

- *Activate on Mac:* source venv/bin/activate
- *Activate on Windows:* .\venv\Scripts\activate

## Server-Setup and Commands

Add a folder "employee_pics" inside the static folder.

Install docker desktop in the first place, then:
- docker-compose build
- docker-compose up

Open 2nd terminal:
- docker-compose run app alembic revision --autogenerate -m "migration" (generate migration of db)
- docker-compose run app alembic upgrade head (upgrade db to latest version)

(**IF** no alembic in filesystem -> docker-compose alembic init alembic)

Open browser

-> 127.0.0.1:5050 -> register db to access via pgAdmin (login -> .env file)
-> 127.0.0.1:8080/ for WebApp and API
-> it is necessary to add the IP address of the local machine in Unity for the Hololens app to work properly


## Requirements
Not necessary with docker setup.

- *Install:* venv/bin/python -m pip install -r requirements.txt
- *Generate:* venv/bin/python -m pip freeze > requirements.txt



