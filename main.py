"""
@title:     KP MCI - Server
@author:    Felix Goebel
@date:      16.10.

"""

import os, sys, io
import face_recognition as facerec
import cv2
import numpy as np
import math
import datetime
import pytz

import PIL.Image as Image

import shutil

from fastapi import FastAPI, Request,File, UploadFile, WebSocket, WebSocketDisconnect
from fastapi.encoders import jsonable_encoder
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi_sqlalchemy import DBSessionMiddleware, db
from fastapi.exceptions import HTTPException

from sqlalchemy import update, delete

from forms import AddEmployeeForm

from starlette.responses import HTMLResponse

from dotenv import load_dotenv, find_dotenv
from models import Employee

# import uvicorn
# from starlette.applications import Starlette



# load environment variables from file
load_dotenv(".env")

# initialize webapp
app = FastAPI()
app.add_middleware(DBSessionMiddleware, db_url=os.environ["DATABASE_URL"])
app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")


# RETURNS FACE-RECOGNITION-CONFIDENCE IN PERCENTAGE
def face_confidence(face_distance, face_match_treshold=0.6):
    range = 1.0 - face_match_treshold
    linear_val  = (1.0 - face_distance) / (range * 2.0)
    if face_distance > face_match_treshold:
        return str(round(linear_val * 100, 2)) + "%"
    else:
        value = (linear_val + ((1.0 - linear_val) * math.pow((linear_val - 0.5) * 2, 0.2))) * 100
        return str(round(value, 2)) + "%"

# FACERECOGNITION CLASS AND METHODS
class FaceRecognition:
    face_locations = []
    face_encodings = []
    face_names = []
    known_face_encodings = []
    known_face_names = []

    seen_in_FR_session = {}

    # CONSTRUCTOR
    def __init__(self):
        self.encode_faces()

    # FACE ENCODINGS
    def encode_faces(self):
        
        for image in os.listdir("static/employee_pics"):
            print(image)

            if (image not in self.known_face_names):
                print("Image not present", image, self.known_face_names)
                face_image = facerec.load_image_file(f'static/employee_pics/{image}')

                try:
                    face_encoding = facerec.face_encodings(face_image)[0]
                except:
                    print("Error: No Face found in picuture.")

                self.known_face_encodings.append(face_encoding)
                self.known_face_names.append(image)
            else:
                print("Image already known and stored in class-variable: known_face_names")
    ####################################################
    # FACERECOGNITION -> MULTIPLE PERSONS              #
    # RETURN EVERY PERSON THAT WAS RECOGNIZED VIA JSON #
    ####################################################
    def run_recognition(self):
        print("run recognition")
        img = cv2.imread("static/face.jpg")
        small_img = cv2.resize(img, (0,0), fx=0.25, fy=0.25)

        self.face_locations = facerec.face_locations(small_img)
        self.face_encodings = facerec.face_encodings(small_img, self.face_locations)

        self.face_names = []

        for face_encoding in self.face_encodings:
            matches = facerec.compare_faces(self.known_face_encodings, face_encoding)
            name = "unknown"
            confidence = "unknown"

            face_distances = facerec.face_distance(self.known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)

            if matches[best_match_index]:
                name = self.known_face_names[best_match_index]
                confidence = face_confidence(face_distances[best_match_index])

            self.face_names.append(f"{name}")

        print("RECOGNIZED FACENAMES: ", self.face_names)
        
        # Add Employees that have been recognized to a list   
        empllist = []

        # get current time with corresponding timezone and proper format
        now = datetime.datetime.now(datetime.timezone.utc)
        tz = pytz.timezone("Europe/Berlin")
        now_tz = now.replace(tzinfo=pytz.utc).astimezone(tz)
        current_time = now_tz.strftime("%d.%m.%Y - %H:%M Uhr")

        # RETURN THE EMPLOYEES FROM DATABASE THAT MATCH THE RECOGNIZED FACES FROM THE IMAGE
        with db():
            
            for name in self.face_names:
                if (name != "unknown"):
                    empl = db.session.query(Employee).where(Employee.picture == f"static/employee_pics/{name}").first()
                    empl_update = {empl.employee_id: current_time}
                    self.seen_in_FR_session.update(empl_update)

                    empllist.append(empl)

        return jsonable_encoder(empllist)

# WEBSOCKET

@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    fr = FaceRecognition()
    await websocket.accept()
    print("CLIENT CONNECTED")
    
    # TRY TO RECEIVE BYTES
    while True:

        try:
            data = await websocket.receive_bytes()
            img = Image.open(io.BytesIO(data))
            img.save("./static/face.jpg")
            nameslist = fr.run_recognition()

        # IF WEBSOCKET GOT CLOSED (BOTH INTENTIONALLY AND UNINTENIONALLY)
        except WebSocketDisconnect:
            # UPDATE TIME_MET FOR ALL PERSONS THAT WERE ENCOUNTERED AT THE END OF THE FACERECOGNITION PROCESS
            with db():
                if len(fr.seen_in_FR_session) > 0:
                    for employee_id in fr.seen_in_FR_session:
                        empl = db.session.query(Employee).where(Employee.employee_id == employee_id).first()
                        empl.last_seen = fr.seen_in_FR_session[employee_id]
                    db.session.commit()
            print("CLIENT DISCONNECTED -> WebSocketDisconnect")
            break
        
        if nameslist != None:
            await websocket.send_json(nameslist)
        
    #websocket.close()

# ROOT
@app.get("/")
async def home(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

# EMPLOYEE LIST
@app.get("/employees")
def get_employees(request: Request):
    employees = db.session.query(Employee.employee_id, Employee.firstname, Employee.lastname, Employee.role, Employee.picture).all()
    return templates.TemplateResponse("employees.html", {"request": request, "employees": employees})

# EMPLOYEE GET REQUEST
@app.get("/add-employee")
async def add_employee(request: Request):
    return templates.TemplateResponse("add-employee.html", {"request": request})

# ADD EMPLOYEE POST REQUEST AND ERROR HANDLING
@app.post("/add-employee")
async def add_employee(request: Request, file: UploadFile = File(...)):
    
    try:
        file_location = f"static/employee_pics/{file.filename}"
        with open(file_location, "wb+") as file_object:
            shutil.copyfileobj(file.file, file_object) 
    except Exception:
        pass
    finally:
        file.file.close()
    
    form = AddEmployeeForm(request)
    await form.load_data()
    
    if await form.is_valid():
        errors = ["success"]
        print("Form has no errors.")

        db_employee = Employee(firstname = form.firstname, lastname = form.lastname, role = form.role, picture = file_location)
        db.session.add(db_employee)
        db.session.commit()

    else:
        print("Form has errors.")
        errors = form.errors

    return templates.TemplateResponse("add-employee.html", {"request": request, "errors": errors})

# INDEX [OPTIONAL] - MIGHT DELETE LATER
@app.get("/index", response_class=HTMLResponse)
def home(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

# API FOR ALL DB ENTRIES -> GRAB FOR MEMORY GAME
@app.get("/api-employees/")
def api_employees():
    employees = db.session.query(Employee.firstname, Employee.lastname, Employee.role, Employee.picture).all()
    return [dict(data._mapping) for data in employees]

#DELETE EMPLOYEES FROM DB AS WELL AS THE CORRESPONDING PICTURES FROM THE STATIC FOLDER
@app.delete("/delete/{id}")
def delete_job(id: int):
    empl = db.session.query(Employee).filter(Employee.employee_id == id).first()
    
    if not empl:
        print("DELETE-STATUS: Employee can not be found.")
        raise HTTPException(status_code=404, detail="Not found")
    
    try: 
        os.remove(empl.picture)
        print(f"DELETE-STATUS: Removed Picture of {empl.firstname} {empl.lastname}  under  {empl.picture}.")
    except:
        print(f"DELETE-STATUS: Failed to remove the picture of {empl.firstname} {empl.lastname}.")
    
    db.session.delete(empl)
    db.session.commit()
    print(f"DELETE-STATUS: {empl.firstname} {empl.lastname} deleted.")
    return {"detail": f"Successfully deleted {empl.firstname} {empl.lastname}. Please <a href=''>refresh</a> this page to view the changes."}

###################################################################
# run as local server without container (pgadmin still necessary) #
###################################################################
#                                                                 #
# if __name__ == "__main__":                                      #
# uvicorn.run(app, host="0.0.0.0", port=8080)                     #
#                                                                 #
###################################################################